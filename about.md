---
layout: page
title: About
---

A bit about me:

I am a geoscientist, materials scientist, and data scientist. My professional interests center around the beautiful things at the intersection of chemistry, crystal symmetry, quantum physics, and electromagnetics. In addition, I have a strong allergy to redoing work (my own or other people's) and prefer that No Data Be Left Behind.

## Github

The projects that I'm involved with on Github (May 2020):

* thermal-expansion: I have started a Python project to interpret thermal expansion data based on a small collaboration with my old master's thesis advisor, Anne Hofmeister. It operates on data from a Netszch dilatometer.
* MetisMTAProject: A regression analysis on the distribution of hospitals relative to New York MTA train stations and traffic. Project 1 for [Metis](https://thisismetis.com).
* MetisBaseball: A regression analysis on Major League Baseball attendance, 1920-2019. Project 2 for [Metis](https://thisismetis.com).
* IgneousEons: A classification analysis to search for patterns in igneous rock chemistry by geologic eon. Project 3 for [Metis](https://thisismetis.com).
